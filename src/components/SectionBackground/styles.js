import styled, { css } from 'styled-components';

const containerBackgroundActive = (theme) => css`
  background-image: url(${theme.bgUrls.rainbowBg});
  background-size: cover;
  color: ${theme.colors.white};
`;

export const Container = styled.div`
  ${({ theme, background }) => css`
    background: url(${theme.bgUrls.snowBg});
    color: ${theme.colors.primaryColor};

    ${background && containerBackgroundActive(theme)};

    min-height: 100vh;
    display: flex;
    align-items: center;
  `}
`;

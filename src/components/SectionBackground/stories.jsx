import { SectionBackground } from '.';
import { SectionContainer } from '../SectionContainer';

export default {
  title: 'SectionBackground',
  component: SectionBackground,
  args: {
    children: (
      <div>
        <h1>Section Background</h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur optio
          excepturi ullam quia repellendus quaerat, vero ab consequatur nobis
          rerum vel modi hic obcaecati suscipit alias voluptatum! Animi, impedit
          doloribus!
        </p>
      </div>
    ),
  },
  argTypes: {
    children: { type: '' },
  },
};

export const Template = (args) => {
  return (
    <div>
      <SectionBackground {...args} />
    </div>
  );
};

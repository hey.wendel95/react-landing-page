import { NavLinks } from '.';
import { screen } from '@testing-library/dom';
import { renderTheme } from '../../styles/render-theme';

import mock from './mock';
import { theme } from '../../styles/theme';

describe('<NavLinks />', () => {
  it('should render Links', () => {
    renderTheme(<NavLinks links={mock}>Children</NavLinks>);
    expect(screen.getAllByRole('link')).toHaveLength(mock.length);
  });

  it('should not render  Links', () => {
    renderTheme(<NavLinks />);
    expect(screen.queryAllByText(/links/i)).toHaveLength(0);
  });

  it('should render Links', () => {
    renderTheme(<NavLinks links={mock} />);
    expect(screen.getByText(/Link 10/i).parentElement).toHaveStyleRule(
      'flex-flow',
      'column wrap',
      {
        media: theme.media.lteMedium,
      },
    );
  });

  it('should match snapshot', () => {
    const { container } = renderTheme(
      <NavLinks links={mock}>Children</NavLinks>,
    );
    expect(container).toMatchSnapshot();
  });
});

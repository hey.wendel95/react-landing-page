export default {
  background: false,
  title: 'My Grid',
  description:
    'Lorem até que é legal mas bora inventar palavras que dá no mesmo, o importante é não deixar a peteca cair.',
  grid: [
    {
      title: 'Teste 1',
      description:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam dolore suscipit explicabo hic optio vitae pariatur, placeat assumenda ab natus maxime vel iste? Necessitatibus iusto, esse reprehenderit dolore et maiores.',
    },
    {
      title: 'Teste 2',
      description:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam dolore suscipit explicabo hic optio vitae pariatur, placeat assumenda ab natus maxime vel iste? Necessitatibus iusto, esse reprehenderit dolore et maiores.',
    },
    {
      title: 'Teste 3',
      description:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam dolore suscipit explicabo hic optio vitae pariatur, placeat assumenda ab natus maxime vel iste? Necessitatibus iusto, esse reprehenderit dolore et maiores.',
    },
  ],
};

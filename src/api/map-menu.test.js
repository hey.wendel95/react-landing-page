import { mapMenu, mapMenuLinks } from './map-menu';

describe('map-menu', () => {
  it('should return a object predefined if there is no data ', () => {
    const menu = mapMenu();
    expect(menu.newTab).toBe(false);
    expect(menu.text).toBe('');
    expect(menu.srcImg).toBe('');
    expect(menu.link).toBe('');
  });

  it('should map menu to match keys and values required', () => {
    const menu = mapMenu({
      open_in_new_tab: false,
      logo_text: 'LOGO 1',
      logo_link: '#home',
      menu: [
        {
          open_in_new_tab: false,
          link_text: 'Pricing',
          url: '#pricing',
        },
        {
          open_in_new_tab: false,
          link_text: 'Contact',
          url: '#contact',
        },
      ],
      logo: {
        url: 'a.svg',
      },
    });
    expect(menu.newTab).toBe(false);
    expect(menu.text).toBe('LOGO 1');
    expect(menu.srcImg).toBe('a.svg');
    expect(menu.link).toBe('#home');
    expect(menu.links[0].newTab).toBe(false);
    expect(menu.links[0].children).toBe('Pricing');
    expect(menu.links[0].link).toBe('#pricing');
  });

  it('should return an empty array if there is no links ', () => {
    const links = mapMenuLinks();
    expect(links).toEqual([]);
  });

  it('should map links if they was passed ', () => {
    const links = mapMenuLinks([
      {},
      {
        open_in_new_tab: false,
        link_text: 'Contact',
        url: '#contact',
      },
    ]);

    expect(links[1].newTab).toBe(false);
    expect(links[1].children).toBe('Contact');
    expect(links[1].link).toBe('#contact');
  });
});

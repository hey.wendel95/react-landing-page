import {
  mapSectionContent,
  mapSections,
  mapSectionTwoColumns,
  mapTextGrid,
  mapImageGrid,
} from './map-sections';

import pagesFakeData from './dados.json';

describe('map-section', () => {
  it('should render predefined section if no data', () => {
    const data = mapSections();
    expect(data).toEqual([]);
  });

  it('should render sections with correct data', () => {
    const data = mapSections(pagesFakeData[0].sections);
    expect(data[0].component).toBe('section.section-two-columns');
  });

  it('should test section with invalid data', () => {
    const withNoTextOrImageGrid = mapSections([
      {
        __component: 'section.section-grid',
      },
    ]);

    const withNoComponent = mapSections([{}]);
    expect(withNoTextOrImageGrid).toEqual([
      { __component: 'section.section-grid' },
    ]);

    expect(withNoComponent).toEqual([{}]);
  });

  it('should test section grid with no text or images', () => {
    const withNoTextOrImageGrid = mapSections([
      {
        __component: 'section.section-grid',
        image_grid: [{}],
      },
      {
        __component: 'section.section-grid',
        text_grid: [{}],
      },
    ]);

    expect(withNoTextOrImageGrid.length).toBe(2);
  });

  it('should map section two columns with empty data', () => {
    const data = mapSectionTwoColumns();
    expect(data.background).toBe(false);
    expect(data.component).toBe('');
    expect(data.sectionId).toBe('');
    expect(data.srcImg).toBe('');
    expect(data.text).toBe('');
    expect(data.title).toBe('');
  });

  it('should map section two columns if there is data', () => {
    const data = mapSectionTwoColumns({
      __component: 'section.section-two-columns',
      title: 'January brings us firefox 85',
      description: 'uma breve descrição',
      metadata: {
        background: true,
        section_id: 'home',
      },
      image: {
        alternativeText:
          'design of peoples holding the CSS, JS and  HTML 5 logos',
        url: 'a.svg',
      },
    });
    expect(data.background).toBe(true);
    expect(data.component).toBe('section.section-two-columns');
    expect(data.sectionId).toBe('home');
    expect(data.srcImg).toBe('a.svg');
    expect(data.text).toBe('uma breve descrição');
    expect(data.title).toBe('January brings us firefox 85');
  });

  it('should map section content with empty data', () => {
    const data = mapSectionContent();
    expect(data.background).toBe(false);
    expect(data.component).toBe('');
    expect(data.sectionId).toBe('');
    expect(data.html).toBe('');
    expect(data.title).toBe('');
  });

  it('should render section content if there is data', () => {
    const data = mapSectionContent({
      __component: 'section.section-content',
      title: 'Intro title',
      content: '<p>um paragrafo</p>',
      metadata: {
        background: false,
        section_id: 'intro',
      },
    });

    expect(data.background).toBe(false);
    expect(data.component).toBe('section.section-content');
    expect(data.sectionId).toBe('intro');
    expect(data.html).toBe('<p>um paragrafo</p>');
    expect(data.title).toBe('Intro title');
  });

  it('should map section text grid with empty data', () => {
    const data = mapTextGrid(undefined);

    expect(data.background).toBe(false);
    expect(data.component).toBe('section.section-grid-text');
    expect(data.sectionId).toBe('');
    expect(data.description).toBe('');
    expect(data.title).toBe('');
  });

  it('should render section text grid if there is data', () => {
    const data = mapTextGrid({
      __component: 'section.section-grid',
      description: 'Breve descrição',
      title: 'My Grid',
      text_grid: [
        {
          title: 'Teste 1',
          description: 'Lorem',
        },
        {
          title: 'Teste 2',
          description: 'Lorem',
        },
      ],
      image_grid: [],
      metadata: {
        background: true,
        section_id: 'grid-one',
      },
    });

    expect(data.background).toBe(true);
    expect(data.component).toBe('section.section-grid-text');
    expect(data.sectionId).toBe('grid-one');
    expect(data.description).toBe('Breve descrição');
    expect(data.title).toBe('My Grid');
    expect(data.grid[0].title).toBe('Teste 1');
    expect(data.grid[0].description).toBe('Lorem');
  });

  it('should map section image grid with empty data', () => {
    const data = mapImageGrid(undefined);

    expect(data.background).toBe(false);
    expect(data.component).toBe('section.section-grid-image');
    expect(data.sectionId).toBe('');
    expect(data.description).toBe('');
    expect(data.title).toBe('');
  });

  it('should render section image grid if there is data', () => {
    const data = mapImageGrid({
      __component: 'section.section-grid',
      title: 'Gallery',
      description: 'Breve descrição',
      text_grid: [],
      image_grid: [
        {
          image: {
            _id: '60f6df2a3d3ff21752cdf549',
            alternativeText: 'An orange drink',
            url: 'cloudinary.img',
          },
        },
      ],
      metadata: {
        background: false,
        name: 'Gallery',
        section_id: 'gallery',
      },
    });

    expect(data.background).toBe(false);
    expect(data.component).toBe('section.section-grid-image');
    expect(data.sectionId).toBe('gallery');
    expect(data.description).toBe('Breve descrição');
    expect(data.title).toBe('Gallery');
    expect(data.grid[0].srcImg).toBe('cloudinary.img');
    expect(data.grid[0].altText).toBe('An orange drink');
  });
});

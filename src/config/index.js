export default {
  url: 'https://strapi-landing-pages-one.herokuapp.com/pages/?slug=',
  siteName: '1st React Page',
  defaultSlug: 'landing-page',
};
